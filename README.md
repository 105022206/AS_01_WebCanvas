# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

*    <img src="assets/eraser.png" width="50px" height="50px"></img>
       * 橡皮擦功能 使用方法：點一下圖案後會切換到橡皮擦功能，當滑鼠按下時觸發事件，
        會根據mouse位置來將畫布塗成原本畫布的顏色，當mouse up時結束事件。
*    <img src="assets/pen.jpg" width="50px" height="50px"></img>
       * 畫筆功能   使用方法：點一下圖案就會切換到畫筆功能，當滑鼠按下時觸發事件，
       會根據mouse位置來將畫布塗成設定好的的顏色，當mouse up時結束事件。顏色可以在
        R、G、B上面調整。
*   <img src='assets/special.png' width="50px" height="50px"></img>
       * 彩色畫筆功能 使用方法：點一下圖案就會切換到彩色畫筆功能，當滑鼠按下時觸發事件，
        會根據mouse位置來將畫布塗成根據gradient變化的顏色，當mouse up時結束事件。
*   <img src='assets/reset2.png' width="50px" height="50px"></img>
       * reset功能 使用方法：點一下圖案就會將整個畫布清成初始畫面。
*   <img src='assets/redo.png' width="50px" height="50px"></img>
       * redo功能 使用方法：點一下圖案就會切換到下一張畫布的樣子（下一個動作結束時的畫面）
       * ，如果沒有使用過undo功能按下redo不會有任何功能。
*   <img src='assets/undo.jpeg' width="50px" height="50px"></img>
       * undo功能 使用方法：點一下圖案就會切換到上一張畫布的樣子（上一個動作結束時的畫面）。
*   <img src='assets/rectangle.jpg' width="50px" height="50px"></img>
       * 矩形圖案功能 使用方法：點一下圖案就會切換到繪製矩形功能，當滑鼠按下時觸發事件，
        會根據mouse位置和滑鼠按下時位置的距離計算長和寬，會先將畫布以滑鼠按下時的位置為原點
        像滑鼠目前位置的方向以矩形清除接著在繪製矩形，當mouse up時結束事件。（遇到的困難是
        當滑鼠往原點方向拉回時會留下許多沒清除乾淨的殘渣，不知道該怎麼處理）
*   <img src='assets/triangle.png' width="50px" height="50px"></img>
       * 三角形圖案功能 使用方法：點一下圖案就會切換到繪製三角形功能，當滑鼠按下時觸發事件，
        會根據滑鼠點下時的位置當作原點，並根據mouse目前位置的x方向和原點x的距離乘以2來當作
三角形底邊長，畫筆會先從原點畫到滑鼠目前位置，接著在畫到目前滑鼠位置-2倍mouse目前位置的x方
向和原點x的距離會後連回原點。（每次畫都會將先前的線條清成背景色，目前遇到的困難還是清理得不夠乾淨。）
*   <img src='assets/circle.jpg' width="50px" height="50px"></img>
       * 圓形圖案功能 使用方法：點一下圖案就會切換到繪製圓形功能，當滑鼠按下時觸發事件，會
       * 以滑鼠按下時的位置當原點，並計算滑鼠目前位置到原點的距離來當作半徑，並以原點為圓心
       * 繪製圓型。（在繪製圖行前會以原點-半徑-1為起點像滑鼠目前方向以矩形清除畫布，和繪製矩形
       * 一樣，都會留下一點渣渣不知道如何處理。）
*   <img src='assets/text.jpg' width="50px" height="50px"></img>
       * 文字功能 使用方法：點一下圖案就會切換到文字功能，可以在畫布上方的text區域輸入文字，
       * 並在右側選好要的字型和字體大小，最後在想要的位置點一下滑鼠就可以繪製文字了。
*   <img src='assets/download.png' width="50px" height="50px"></img>
       * 下載圖片功能 使用方法：點一下圖案可以將目前畫布內容下載。
*   upload
       * 上傳功能 使用方法：點一下選擇檔案可以選擇想要上傳並貼上的圖片，選擇好後就會將圖片貼在
       *畫布上了。
*   cursor變化
       * cursor會根據選取的工具變化（只有在畫布上）。
*   color selector
       * 根據R,G,B 三個拉桿設置RGB 範圍從0~255 混合出來的顏色會顯示在下方區塊
*   brush size
       * 調整拉桿可以調整筆刷大小。
*   font size and style
       * 從option中選取大小和字體套用在text上。



    

