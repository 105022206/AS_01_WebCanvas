var canvas=document.getElementById('mycanvas');
var ctx =canvas.getContext('2d');

var color =['red','blue','black'];
var Sarray = new Array();
var step= -1;
var last_mousex = last_mousey = 0;
var download = document.getElementById("download");

var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
pushConvas();

///get mouse position


/// special painting pen
document.getElementById('special').addEventListener('click',function(){
    canvas.addEventListener('mousedown',sp,false);
    document.getElementById('content').style.cursor="url('../assets/spo706.cur'),pointer";
    canvas.removeEventListener('mousedown', painting_eraser,false);
    canvas.removeEventListener('mousedown', painting_rectangle,false);
    canvas.removeEventListener('mousedown', Text,false);
    canvas.removeEventListener('mousedown', painting_Triangle,false);
    canvas.removeEventListener('mousedown', painting_Circle,false);
    canvas.removeEventListener('mousedown',painting_pen,false);
},false);
/// reset 
document.getElementById('reset').addEventListener('click',function(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    //document.getElementById('content').style.cursor="default";
    //pushConvas();
},false);
/// color palette
document.getElementById('redd').addEventListener('mousemove',function(){
    console.log('R');
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    document.getElementById('red').style.backgroundColor='rgb('+_R+','+_G+','+_B+')';
},false);
document.getElementById('blued').addEventListener('mousemove',function(){
    console.log('B');
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    document.getElementById('red').style.backgroundColor='rgb('+_R+','+_G+','+_B+')';
},false);
document.getElementById('greend').addEventListener('mousemove',function(){
    console.log('G');
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    document.getElementById('red').style.backgroundColor='rgb('+_R+','+_G+','+_B+')';
},false);
document.getElementById('size').addEventListener('mousemove',function(){
    console.log('size');
    _size=document.getElementById('size').value;
    ctx.lineWidth=_size;
},false);

document.getElementById('redo').addEventListener('click',function(){redo();},false);
document.getElementById('pen').addEventListener('click',function(){
    console.log('pen');
    document.getElementById('content').style.cursor="url('assets/pen.cur'),pointer";
    canvas.addEventListener('mousedown',painting_pen,false);
    canvas.removeEventListener('mousedown', painting_eraser,false);
    canvas.removeEventListener('mousedown', painting_rectangle,false);
    canvas.removeEventListener('mousedown', Text,false);
    canvas.removeEventListener('mousedown', painting_Triangle,false);
    canvas.removeEventListener('mousedown', painting_Circle,false);
    canvas.removeEventListener('mousedown', sp,false);
},false);
document.getElementById('undo').addEventListener('click',function(){undo();},false);
document.getElementById('eraser').addEventListener('click',function(){
    console.log('eraser');
    document.getElementById('content').style.cursor="url('assets/cursor.cur'),pointer";
    canvas.addEventListener('mousedown',painting_eraser,false);
    canvas.removeEventListener('mousedown', painting_pen,false);
    canvas.removeEventListener('mousedown', painting_rectangle,false);
    canvas.removeEventListener('mousedown', Text,false);
    canvas.removeEventListener('mousedown', painting_Triangle,false);
    canvas.removeEventListener('mousedown', painting_Circle,false);
    canvas.removeEventListener('mousedown', sp,false);
},false);
document.getElementById('rectangle').addEventListener('click',function(){
    console.log('rectangle');
    document.getElementById('content').style.cursor="url('assets/3D rectangle.cur'),pointer";
    canvas.addEventListener('mousedown',painting_rectangle,false);
    canvas.removeEventListener('mousedown', painting_pen,false);
    canvas.removeEventListener('mousedown', painting_eraser,false);
    canvas.removeEventListener('mousedown', Text,false);
    canvas.removeEventListener('mousedown', painting_Triangle,false);
    canvas.removeEventListener('mousedown', painting_Circle,false);
    canvas.removeEventListener('mousedown', sp,false);
},false);

document.getElementById('circle').addEventListener('click',function(){
    console.log('Circle');
    document.getElementById('content').style.cursor="url('assets/Circle.cur'),pointer";
    canvas.addEventListener('mousedown',painting_Circle,false);
    canvas.removeEventListener('mousedown', painting_pen,false);
    canvas.removeEventListener('mousedown', painting_eraser,false);
    canvas.removeEventListener('mousedown', Text,false);
    canvas.removeEventListener('mousedown', painting_Triangle,false);
    canvas.removeEventListener('mousedown', painting_rectangle,false);
    canvas.removeEventListener('mousedown', sp,false);
},false);

document.getElementById('triangle').addEventListener('click',function(){
    console.log('triangle');
    document.getElementById('content').style.cursor="url('assets/Blue Triangle.cur'),pointer";
    canvas.addEventListener('mousedown',painting_Triangle,false);
    canvas.removeEventListener('mousedown', painting_pen,false);
    canvas.removeEventListener('mousedown', painting_eraser,false);
    canvas.removeEventListener('mousedown', Text,false);
    canvas.removeEventListener('mousedown', painting_Circle,false);
    canvas.removeEventListener('mousedown', painting_rectangle,false);
    canvas.removeEventListener('mousedown', sp,false);
},false);


document.getElementById('text_btn').addEventListener('click',function(){
    console.log('text');
    document.getElementById('content').style.cursor="text";
    canvas.addEventListener('mousedown',Text,false);
    canvas.removeEventListener('mousedown', painting_pen,false);
    canvas.removeEventListener('mousedown', painting_eraser,false);
    canvas.removeEventListener('mousedown', painting_rectangle,false);
    canvas.removeEventListener('mousedown', painting_Triangle,false);
    canvas.removeEventListener('mousedown', painting_Circle,false);
    canvas.removeEventListener('mousedown', sp,false);
},false);
canvas.addEventListener('mouseup',function(){
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    ctx.strokeStyle='rgb('+_R+','+_G+','+_B+')';
    canvas.removeEventListener('mousemove', mousemove, false);
    canvas.removeEventListener('mousemove', rectangle,false);
    canvas.removeEventListener('mousemove', Triangle,false);
    canvas.removeEventListener('mousemove', clean,false);
    canvas.removeEventListener('mousemove', Circle,false);
    //canvas.removeEventListener('mousedown', sp,false);
    
   


   // canvas.removeEventListener('mousedown', mousemove, false);
    pushConvas();
    console.log('finish',step);
},false);


function getPos(canvas,evt){
    var rect = canvas.getBoundingClientRect(), // abs. size of element
      scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
      scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y
  return {
    x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
    y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
  }
/// paint when mouse move
}
function mousemove(evt){
    var mouse=getPos(canvas,evt);
    //console.log(mouse.x,mouse.y);
    ctx.lineTo(mouse.x,mouse.y);
    ctx.stroke();
}

/// 
function sp(evt){
    var mouse=getPos(canvas,evt);
        var gradient=ctx.createLinearGradient(0,0,canvas.width,0);
        gradient.addColorStop("0","green");
        gradient.addColorStop("0.4","blue");
        gradient.addColorStop("0.8","red");
        console.log('special');
        ctx.strokeStyle=gradient;
        ctx.beginPath();
        ctx.moveTo(mouse.x,mouse.y);
        //evt.preventDefault();
        canvas.addEventListener('mousemove',mousemove,false);
}

function painting_pen(evt){
    
        _R=document.getElementById('redd').value;
        _G=document.getElementById('greend').value;
        _B=document.getElementById('blued').value;
        var mouse=getPos(canvas,evt);
        ctx.strokeStyle='rgb('+_R+','+_G+','+_B+')';
        ctx.beginPath();
        ctx.lineCap = "round";
        ctx.moveTo(mouse.x,mouse.y);
        //evt.preventDefault();
        canvas.addEventListener('mousemove',mousemove,false);
    
    
}


function painting_eraser(evt){
   
        var mouse=getPos(canvas,evt);
        ctx.strokeStyle='#ebebeb';
        ctx.beginPath();
        ctx.moveTo(mouse.x,mouse.y);
        //evt.preventDefault();
        canvas.addEventListener('mousemove',mousemove,false);
    
};

/// 存取當前的畫布，之後用來還原 
function pushConvas(){
    step++;
    //console.log('arraysize',Sarray.length,step);
    if(step<Sarray.length){step=Sarray.length;}
    Sarray.push(document.getElementById('mycanvas').toDataURL());
}
/// 回上一步
function undo(){
    console.log("undo",step,'lenth',Sarray.length);
    if (step > 0) {
        step--;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var canvasPic = new Image();
        canvasPic.src = Sarray[step];
        canvasPic.onload = function () { console.log("EE");ctx.drawImage(canvasPic,0,0); }
    }

}
/// 回下一步
function redo(){
    console.log("redo",step,'l',Sarray.length);

    if(step<Sarray.length-1){
        step++;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var canvasPic = new Image();
        canvasPic.src= Sarray[step];
        canvasPic.onload = function(){ ctx.drawImage(canvasPic,0,0);}
    }
}

function painting_rectangle(evt){
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    ctx.strokeStyle='rgb('+_R+','+_G+','+_B+')';
        var mouse=getPos(canvas,evt);
        last_mousex=mouse.x;
        last_mousey=mouse.y;
        canvas.addEventListener('mousemove',clean,false);
        canvas.addEventListener('mousemove',rectangle,false);
    
    



}
function clean(evt){
    var mouse=getPos(canvas,evt);
    var width=mouse.x-last_mousex;
    var height=mouse.y-last_mousey;

    ctx.clearRect(last_mousex, last_mousey, width, height);
}

function rectangle(evt){
    
    var mouse=getPos(canvas,evt);
    var width=mouse.x-last_mousex;
    var height=mouse.y-last_mousey;
    ctx.strokeRect(last_mousex, last_mousey, width, height);
    
    //ctx.clearRect(mouse.x, last_mousey, -width, height);
    //ctx.clearRect(last_mousex, mouse.y, width, -height);
    
    
    //console.log('2',mouse.x,mouse.y)

    

}

function download() {
    download.setAttribute("href", image);
    //download.setAttribute("download","archive.png");
    }



function handleImage(e){
    console.log("her");
    var reader = new FileReader();
    reader.onload = function(event){
    var img = new Image();
    img.onload = function(){
        canvas.width = img.width;
        canvas.height = img.height;
        console.log('draw');
        ctx.drawImage(img,0,0);
    }
    img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}

function Text(evt){
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    var mouse=getPos(canvas,evt);
    ctx.fillStyle='rgb('+_R+','+_G+','+_B+')'
    var font_style=document.getElementById('text_font').value;
    var font_size=document.getElementById('text_size').value;
    ctx.font=font_size+'px '+font_style;
    console.log(font_size,font_style);

    console.log(document.getElementById('text').value)
    var text_value=document.getElementById('text').value;
    ctx.fillText(text_value,mouse.x,mouse.y);
    document.getElementById('text').value='';

}

function painting_Circle(evt){
    console.log('ee')
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    ctx.strokeStyle='rgb('+_R+','+_G+','+_B+')';
        var mouse=getPos(canvas,evt);
        last_mousex=mouse.x;
        last_mousey=mouse.y;
        //ctx.strokeStyle='#ebebeb';
        //ctx.beginPath();
        //ctx.moveTo(mouse.x,mouse.y);
        //evt.preventDefault();
        canvas.addEventListener('mousemove',Circle,false);
    


}
function Circle(evt){
    console.log('ee')
    var mouse=getPos(canvas,evt);
    console.log(last_mousex,mouse.x)
    var width=mouse.x-last_mousex;
    var height=mouse.y-last_mousey;
    var radius=Math.sqrt(width*width+height*height);
    console.log(width,height)
    ctx.beginPath();
    ctx.clearRect(last_mousex - radius - 1, last_mousey - radius - 1, radius * 2 + 2, radius * 2 + 2);
    ctx.closePath();
    ctx.beginPath();
    ctx.arc(last_mousex, last_mousey, radius, 0, 2 * Math.PI);
    ctx.stroke();
    



}
function painting_Triangle(evt){
    /*
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    ctx.strokeStyle='rgb('+_R+','+_G+','+_B+')';*/
    //ctx.strokeStyle='black';
        var mouse=getPos(canvas,evt);
        last_mousex=mouse.x;
        last_mousey=mouse.y;
        
        ctx.beginPath();
        //ctx.lineCap = "round";
        ctx.moveTo(mouse.x,mouse.y);
        console.log("ss");


        //evt.preventDefault();
        canvas.addEventListener('mousemove',Triangle,false);

}
function Triangle(evt){
    _R=document.getElementById('redd').value;
    _G=document.getElementById('greend').value;
    _B=document.getElementById('blued').value;
    ctx.strokeStyle='rgb('+_R+','+_G+','+_B+')';
    
    var mouse=getPos(canvas,evt);
    console.log(last_mousex,mouse.x)
    var width=mouse.x-last_mousex;
    var height=mouse.y-last_mousey;
    console.log(width)
    //ctx.lineCap = "round";
    //ctx.strokeStyle='black';
    ctx.lineTo(mouse.x,mouse.y);
    ctx.lineTo(mouse.x-width*2,mouse.y);
    ctx.lineTo(last_mousex,last_mousey);
    ctx.stroke();
    
    ctx.strokeStyle='#ebebeb';
    ctx.beginPath();
    ctx.moveTo(last_mousex,last_mousey);
    ctx.lineTo(mouse.x,mouse.y);
    ctx.lineTo(mouse.x-width*2,mouse.y);
    ctx.lineTo(last_mousex,last_mousey);
    ctx.stroke();
    //ctx.clearRect(last_mousex, last_mousey, width, height);

}

